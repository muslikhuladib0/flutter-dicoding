import 'package:coba1/page4.dart';
import 'package:flutter/material.dart';

class Page3 extends StatefulWidget {
  @override
  _Page3State createState() => _Page3State();
}

class _Page3State extends State<Page3> {
  GlobalKey _keyRed = GlobalKey();
  _getSizes() {
    final RenderBox renderBoxRed = _keyRed.currentContext.findRenderObject();
    final sizeRed = renderBoxRed.size;
    print("SIZE of Red: $sizeRed");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              // height: screenSize(context).height / 5.5,
              margin: new EdgeInsets.only(top: 30),
              padding: new EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: new Row(children: <Widget>[
                Expanded(
                  child: Container(
                    // height: ,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset(
                        'img/moonchild.jpg',
                      ),
                    ),
                  ),
                ),
                Expanded(
                  // flex: 3,
                  child: Container(
                    key: _keyRed,
                    // constraints: ,
                    // height: box.size.height,
                    // padding: new EdgeInsets.all(10),
                    padding: new EdgeInsets.symmetric(vertical: 65),
                    color: Colors.black,
                    margin: new EdgeInsets.only(left: 8),
                    child: Column(
                      children: [
                        Center(
                          child: new Text(
                            'bulan malam',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        Padding(
                          padding: new EdgeInsets.all(5),
                        ),
                        Center(
                          child: new Text(
                            'sebuah bulan yang canmmmmmmmmmmmtik shot diambil dari sukorame, mangunan, dlingo, bantul, di yogyakarta, indonesia.',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ]),
            ),
            MaterialButton(
              elevation: 5.0,
              padding: EdgeInsets.all(15.0),
              color: Colors.grey,
              child: Text("Get Sizes hasil munxul di debug console"),
              onPressed: _getSizes,
            ),
            RaisedButton(
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return Page4();
                }));
              },
              child: Text('ke page 4'),
            ),
          ],
        ),
      ),
    );
  }
}

//
