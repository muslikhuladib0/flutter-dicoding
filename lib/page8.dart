import 'package:flutter/material.dart';

class Page8 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
    // Contstrainnya whnya paling maximal..menuhi parent widgetnya karena tidak punya child widget
    // return Container(
    //   color: Colors.red[400],
    // );
    // kalau ini constraintnya ikut child widgetnya..
    // return Container(
    //   color: Colors.red[400],
    //   child: Text('ello world'),
    // );
    // kalau widget center dia menuhin parent widgetnya, untuk childnya center default nya di tengah..karna kalau liat code class centernya extend ke align..bisa diganti topleft/left dll
    // return Center(
    //   child: Container(
    //     color: Colors.red[400],
    //     child: Text('ello world'),
    //   ),
    // );
    // bisa buat ngatur wh constraintnya..satunya lagi UnconstrainBox..
    //  return ConstrainedBox(
    //   constraints: BoxConstraints(
    //     minWidth: 70,
    //     minHeight: 70,
    //     maxWidth: 150,
    //     maxHeight: 150,
    //   ),
    //   child: Container(
    //     color: Colors.red,
    //   ),
    // );
    // return UnconstrainedBox(
    //   child: Container(
    //     width: 200,
    //     height: 200,
    //     color: Colors.red,
    //   ),
    // );
  }
}
