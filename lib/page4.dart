// import 'package:coba1/page2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class Page4 extends StatefulWidget {
  @override
  _Page4State createState() => _Page4State();
}

class _Page4State extends State<Page4> {
  TextEditingController inputTextController = TextEditingController();
  String inputan = 'masih kosong';
  int angka = 0;
  bool lampu = false;
  String langFav = '';
  bool macos = false;
  bool linux = false;
  bool windows = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: SpeedDial(
        marginRight: 18,
        marginBottom: 20,
        animatedIcon: AnimatedIcons.menu_close,
        animatedIconTheme: IconThemeData(size: 22.0),
        child: Icon(Icons.add),
        visible: true,
        closeManually: false,
        curve: Curves.bounceIn,
        overlayColor: Colors.black,
        overlayOpacity: 0.5,
        onOpen: () => print('OPENING DIAL'),
        onClose: () => print('DIAL CLOSED'),
        tooltip: 'Speed Dial',
        heroTag: 'speed-dial-hero-tag',
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 8.0,
        shape: CircleBorder(),
        children: [
          SpeedDialChild(
              child: Icon(Icons.accessibility),
              backgroundColor: Colors.red,
              label: 'Back',
              labelStyle: TextStyle(fontSize: 18.0),
              onTap: () => {Navigator.pop(context)}),
          SpeedDialChild(
            child: Icon(Icons.brush),
            backgroundColor: Colors.blue,
            label: 'Ke Page2',
            labelStyle: TextStyle(fontSize: 18.0),
            onTap: () => {Navigator.pushNamed(context, '/page2')},
          ),
          SpeedDialChild(
            child: Icon(Icons.keyboard_voice),
            backgroundColor: Colors.green,
            label: 'Third',
            labelStyle: TextStyle(fontSize: 18.0),
            onTap: () => print('THIRD CHILD'),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: new EdgeInsets.only(
                top: 30,
              ),
            ),
            new Text(
              inputan,
              style: new TextStyle(
                fontFamily: 'Sans',
                fontSize: 20,
              ),
            ),
            new Container(
              padding: new EdgeInsets.symmetric(horizontal: 40),
              child: new TextField(
                // controller: _inputTextController,
                onSubmitted: (value) {
                  setState(() {
                    angka++;
                    inputan = value + angka.toString();
                  });
                },
              ),
            ),
            new Container(
              padding: new EdgeInsets.symmetric(horizontal: 40),
              child: new TextField(
                controller: inputTextController,
              ),
            ),
            new Container(
              child: new Switch(
                  activeColor: Colors.red[300],
                  value: lampu,
                  onChanged: (bool value) {
                    setState(() {
                      lampu = value;
                      if (value) {
                        inputan = 'lampu nyala';
                      } else {
                        inputan = 'lampu mati';
                      }
                    });
                  }),
            ),
            ListTile(
              leading: new Radio(
                  value: 'Dart',
                  groupValue: langFav,
                  onChanged: (String value) {
                    setState(() {
                      langFav = value;
                      inputan = langFav;
                    });
                  }),
              title: Text('Dart'),
            ),
            ListTile(
              leading: new Radio(
                  value: 'Javascript',
                  groupValue: langFav,
                  onChanged: (String value) {
                    setState(() {
                      langFav = value;
                      inputan = langFav;
                    });
                  }),
              title: Text('Javascript'),
            ),
            ListTile(
              leading: new Checkbox(
                  value: macos,
                  onChanged: (bool value) {
                    setState(() {
                      macos = value;
                      if (value) {
                        inputan = 'u is macos user';
                      } else {
                        inputan = 'u not macos user';
                      }
                    });
                  }),
              title: Text('mac os user'),
            ),
            ListTile(
              leading: new Checkbox(
                  value: linux,
                  onChanged: (bool value) {
                    setState(() {
                      linux = value;
                      if (value) {
                        inputan = 'u is linux user';
                      } else {
                        inputan = 'u not linux user';
                      }
                    });
                  }),
              title: Text('linux user'),
            ),
            ListTile(
              leading: new Checkbox(
                  value: windows,
                  onChanged: (bool value) {
                    setState(() {
                      windows = value;
                      if (value) {
                        inputan = 'u is windows user';
                      } else {
                        inputan = 'u not windows user';
                      }
                    });
                  }),
              title: Text('windows user'),
            ),
            new Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(color: Colors.black54, offset: Offset(-5, 19))
                ],
              ),
              margin: new EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: new Image.asset(
                  'img/moonchild.jpg',
                  // width: 200,
                ),
              ),
            ),
            new Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(color: Colors.black54, offset: Offset(-5, 19))
                ],
              ),
              height: 200,
              width: 400,
              margin: new EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: new Image.network(
                  'https://res.cloudinary.com/duh6epdw5/image/upload/v1599869433/kisspng-indomie-instant-noodle-mie-goreng-fried-noodles-ch-5b556b67ebfa77.4168400515323247119666_ixqakv.png',
                  // fit: BoxFit.,
                  // height: 900,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
