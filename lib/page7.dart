import 'package:flutter/material.dart';

class Page7 extends StatelessWidget {
  final textController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            TextField(
              controller: textController,
              decoration: InputDecoration(labelText: 'isi namamu'),
            ),
            RaisedButton(
                child: Text('kirim'),
                onPressed: () {
                  Navigator.pop(context, textController.text);
                }),
          ],
        ),
      ),
    );
  }
}
