import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class Page5 extends StatefulWidget {
  @override
  _Page5State createState() => _Page5State();
}

class _Page5State extends State<Page5> {
  List<int> numberList = <int>[0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  List color = [
    Colors.red,
    Colors.blue,
    Colors.black,
    Colors.yellow,
    Colors.red,
    Colors.blue,
    Colors.black,
    Colors.red,
    Colors.blue,
    Colors.black,
  ];

  int nombor = 1;

  listview(nombor) {
    if (nombor == 1) {
      return ListView(
        children: numberList.map((e) {
          return Container(
            color: color[e],
            height: MediaQuery.of(context).size.height * 0.5,
            child: Center(
              child: Text(
                (e + 1).toString(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 50,
                ),
              ),
            ),
          );
        }).toList(),
      );
    } else {
      return ListView(
        children: List.generate(numberList.length, (index) {
          return Container(
            color: Colors.red,
            height: MediaQuery.of(context).size.height * 0.5,
            child: Center(
              child: Text(
                (index + 1).toString(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 50,
                ),
              ),
            ),
          );
        }).toList(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // body: ListView(
      //   children: numberList.map((e) {
      //     return Container(
      //       color: color[e],
      //       height: MediaQuery.of(context).size.height * 0.5,
      //       child: Center(
      //         child: Text(
      //           (e + 1).toString(),
      //           style: TextStyle(
      //             color: Colors.white,
      //             fontSize: 50,
      //           ),
      //         ),
      //       ),
      //     );
      //   }).toList(),
      // ),
      floatingActionButton: SpeedDial(
        child: Icon(Icons.ac_unit),
        children: [
          SpeedDialChild(
              child: Icon(Icons.all_inbox),
              label: 'merah semua',
              onTap: () {
                setState(() {
                  nombor = 0;
                });
              }),
          SpeedDialChild(
              child: Icon(Icons.article_rounded),
              label: 'warna warni',
              onTap: () {
                setState(() {
                  nombor = 1;
                });
              }),
        ],
      ),
      body: listview(nombor),
    );
  }
}
