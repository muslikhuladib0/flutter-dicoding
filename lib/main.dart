import 'package:coba1/codelab1.dart';
import 'package:coba1/codelab2.dart';
import 'package:coba1/custom_widget.dart';
import 'package:coba1/latihan_gesture.dart';
import 'package:coba1/page2.dart';
import 'package:coba1/page3.dart';
import 'package:coba1/page4.dart';
import 'package:coba1/page5.dart';
import 'package:coba1/page6.dart';
import 'package:coba1/page7.dart';
import 'package:coba1/page9.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:coba1/page1.dart';
import 'package:coba1/testing-code.dart';
// import 'dart:js';

void main() {
  runApp(
    new MaterialApp(
      title: 'belajar flutter dicoding',
      theme: ThemeData(
        fontFamily: 'Sans',
      ),
      home: Beranda(),
      routes: {
        '/page1': (context) => new Page1('muslikhuladib'),
        '/page2': (context) => new Page2(
              'muslikhuladib',
              umur: 90,
            ),
        '/page3': (context) => new Page3(),
        '/codelab1': (context) => new DetailScreen(),
        '/page4': (context) => new Page4(),
        '/codelab2': (context) => new DetailScreen2(),
        '/testing': (context) => new MyApp1(),
        '/page5': (context) => new Page5(),
        '/page6': (context) => new Page6(),
        '/page7': (context) => new Page7(),
        '/page9': (context) => new Page9(),
        '/latihangestures': (context) => new LatihanGestures(),
        '/customwidget': (context) => new CustomWidget(),
      },
    ),
  );
}

class Beranda extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('learn flutter from dicoding'),
        backgroundColor: Colors.black,
      ),
      body: Column(
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Button('page 1', '/page1', 'ini data string dari button'),
                Button('page 2', '/page2'),
                Button('page 3', '/page3'),
                Button('page4', '/page4'),
                Button('test code', '/testing'),
                Button('page 5', '/page5'),
                Button('page 6', '/page6'),
                Button('page 9', '/page9'),
              ],
            ),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Button('codelab1', '/codelab1'),
                Button('codelab2', '/codelab2'),
                Button('latihan gestures', '/latihangestures'),
                Button('custom widget', '/customwidget'),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class Button extends StatelessWidget {
  final String nama;
  final String route;
  final String data;

  Button(this.nama, this.route, [this.data]);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.only(left: 10),
      child: new RaisedButton(
        onPressed: () => {
          Navigator.pushNamed(context, route),
        },
        child: new Text(nama),
      ),
    );
  }
}
