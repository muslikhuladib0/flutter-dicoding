import 'package:flutter/material.dart';

class Page2 extends StatelessWidget {
  Page2(this.nama, {this.umur});
  final String nama;
  final int umur;
  final int sizetext = 10;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: new Text('page 2 flutter indonesia'),
        ),
        body: Column(
          children: [
            Padding(
              padding: new EdgeInsets.only(left: 10),
            ),
            new Text(nama),
            new Text(umur.toString()),
            Center(
              child: new RaisedButton(
                onPressed: () {},
                child: new Text('ubah'),
              ),
            ),
          ],
        ));
  }
}
