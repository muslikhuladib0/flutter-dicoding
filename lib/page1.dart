import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Page1 extends StatefulWidget {
  String nama;
  int umur;
  Page1(this.nama, [this.umur]);
  @override
  _Page1State createState() => _Page1State();
}

class _Page1State extends State<Page1> {
  double ukuran = 20;
  Color warna = Colors.blue[300];

  @override
  Widget build(BuildContext context) {
    final String data = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: new AppBar(
        title: new Text('page 1'),
      ),
      body: Container(
        padding: new EdgeInsets.only(top: 20),
        width: MediaQuery.of(context).size.width / 2,
        height: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(
            left: MediaQuery.of(context).size.width / 4, top: 5),
        decoration: BoxDecoration(
          color: Colors.yellow[300],
          shape: BoxShape.rectangle,
          boxShadow: [
            BoxShadow(
              offset: Offset(-41, 30),
              color: Colors.red[400],
            ),
            BoxShadow(
              color: Colors.blue[400],
              offset: Offset(-21, 10),
            ),
          ],
          border: Border.all(
            color: Colors.blue[900],
            width: 10,
          ),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            bottomRight: Radius.circular(15),
            bottomLeft: Radius.circular(5),
            topRight: Radius.circular(5),
          ),
        ),
        child: Column(
          children: [
            new Center(
              child: new Text(
                'nama = ' + widget.nama,
              ),
            ),
            new Center(
              child: new Text('umur = ' + widget.umur.toString()),
            ),
            Text(
              'ssa',
              style: new TextStyle(fontSize: ukuran, color: warna),
            ),
            Center(
              child: Column(
                children: [
                  new RaisedButton(
                    onPressed: () {
                      setState(() {
                        if (ukuran > 20) {
                          ukuran = 20;
                          warna = Colors.blue[300];
                        } else {
                          warna = Colors.red[400];
                          ukuran = 25;
                        }
                      });
                    },
                    child: new Text('ubah'),
                  ),
                  new FlatButton(
                    color: Colors.red[400],
                    textColor: Colors.red[500],
                    disabledColor: Colors.grey,
                    disabledTextColor: Colors.black,
                    padding: EdgeInsets.all(8.0),
                    splashColor: Colors.blueAccent,
                    onPressed: () {},
                    child: new Text('button false'),
                  ),
                  new IconButton(
                    icon: Icon(Icons.access_alarm),
                    onPressed: null,
                  ),
                  new DropdownButton(
                    items: <DropdownMenuItem<String>>[
                      DropdownMenuItem(
                        value: '1juta',
                        child: new Text('1juta'),
                      ),
                      DropdownMenuItem(
                        value: '10juta',
                        child: new Text('10juta'),
                      ),
                    ],
                    value: '10juta',
                    hint: Text('pilih'),
                    onChanged: (String value) {},
                  ),
                ],
              ),
            ),
            Center(child: Text(data)),
          ],
        ),
      ),
    );
  }
}
